class Friend {
	constructor(name){
		this.name = name; 
		this.spouse = '';
		this.children = [];
		this.job = '';
	}

	marry(man){
		var man = prompt( 'Write the name of new spouse of ', man );
		if (this.spouse) {
			delete this.spouse
		};
		this.spouse = man;
	}

	divorse() {
		delete this.spouse;
	}

	hire(newJob) {
		var newJob = prompt( 'Write the name of new job: ', newJob );
		if (this.job) {
			delete this.job
		};
		this.job = newJob;
	}

	fire() {
		delete this.job;
	}

	enslave(newChildren) {
		var quantityOfChildren = prompt( 'Write the quantity of children to add: ', quantityOfChildren );
		var newChildren = [];
		for (var i = 0; i < quantityOfChildren; i++ ){
			var child = prompt( "Kid's name: ", child );
			newChildren.push( child );	
		};
		for (var kid in newChildren) {
			this.children.push(newChildren[kid]); 
		}
		console.log(this.children);
	}	
}

let Anna = new Friend("Anna");
let Din = new Friend("Din");
let Kate = new Friend("Kate");
let Tom = new Friend("Tom");
let Luke = new Friend("Luke");

Anna.marry();
Anna.divorse();
Anna.hire();
Anna.enslave();

Din.marry();
Din.hire();
Din.fire();
Din.enslave();
console.log(Din);
console.log(Anna);